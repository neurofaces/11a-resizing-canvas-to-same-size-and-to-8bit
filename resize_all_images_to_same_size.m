function folder_results_cropped=resize_all_images_to_same_size(folder_imgs, extension)

t=tic;
% run_face_parameterization
crop = true;
prcnt_cropped_H = 0.8; % 0.8 is the value used to create images inside 
% cfd-boy-neutral-allraces\eyes-without-mask-iris-aligned-middle-MAX-size\same-size-top-cropped
% which are good.
recompute_sizes = true;

pri = priority('h');

% folder_imgs = 'C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\cfd-boy-neutral-allraces\eyes-without-mask\';
% folder_imgs = 'C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\cfd-boy-neutral-allraces\eyes-without-mask-iris-aligned-middle-MAX-size\';
% folder_imgs = 'C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\cfd-boy-neutral-allraces\eyes-without-mask-iris-aligned-middle-min-size\';
% % % folder_imgs = 'D:\DOCTORADO\01_DATA\_NoGIT_common_dbs\cfd\_ewom-iris-aligned-MAX-size\';
% % % folder_results = fullfile(folder_imgs, 'same-size\');
% % % if(~exist(folder_results, 'dir')), mkdir(folder_results); end;
folder_results_cropped = fullfile(folder_imgs, '_same-size-top-cropped\');
if(~exist(folder_results_cropped, 'dir')), mkdir(folder_results_cropped); end;

% extension = '.jpg';
% extension = '.bmp';

% list all 'extension' files in 'folder_imgs'
img_filenames = dir([folder_imgs '\*' extension]);
n_img_filenames = numel(img_filenames);

% find sizes of all iris images
sizes = find_sizes_of_all_images(folder_imgs, img_filenames, extension, recompute_sizes);

% load IRIS_CENTROID
% load('E:\DOCTORADO\01_DATA\9-face-parametrization\9c-face-parametrization-9b-bad-iris-manually\allparameters.mat');
% iris_centroids = {allparameters.eyes.iris_centroid}';
% iris_centroids = reshape([iris_centroids{:}], 2, numel(iris_centroids))';
% 
% % find distance of each iris from iris' centroid to top of the image
% di2top = sizes(:,1)-iris_centroids(:,1);
% medium_img_height = round(mean(di2top) + 2*std(di2top));

% stablish new size for all images
H = mode(sizes(:,1));
W = mode(sizes(:,2));
new_size = [H W];

% start the loop for every image in the list to resize images
% hw = waitbar(0, 'Resizing images... 0%');
parfor nf=1:n_img_filenames
    
    %waitbar(nf/n_img_filenames, hw, sprintf('Resizing images... %.2f%%', nf/n_img_filenames*100));
    fprintf('Resizing image %d/%d\n', nf, n_img_filenames);
    
    % % Image filename
    filename = strrep(img_filenames(nf).name, extension, '');
    
    % % Load image
    img = imread(fullfile(folder_imgs, [filename extension]));
    
    % % Resize
	resized_img = resize_image_canvas(img, new_size, 'center');
    
    % % Save image       
% % %     imwrite(resized_img, fullfile(folder_results, strcat(filename, '.bmp')));
    
    % % Addon
    % % Crop top of the image?
    if(crop)
        cropped_img = resize_image_canvas(resized_img, [round(prcnt_cropped_H*H) W], 'bottom');
        imwrite(cropped_img, fullfile(folder_results_cropped, strcat(filename, extension)));
    end

end
disp('Execution ended.');
toc(t)

% close(hw);

priority(pri);