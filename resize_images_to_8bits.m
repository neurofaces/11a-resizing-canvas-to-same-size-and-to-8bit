% resize images to 34x18

% size8bits = [18 34];
% sizes = [18 34] [36 68]
width = 34;
height = 18;
size8bits = [height width];
size_imgs = [num2str(width) 'x' num2str(height)];
method = 'median128';

folder_imgs = ['C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\cfd-boy-neutral-allraces\eyes-without-mask\same-size\homogenized_' method '\'];
folder_imgs = 'C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\cfd-boy-neutral-allraces\eyes-without-mask-iris-aligned-middle-MAX-size\same-size-top-cropped\';
folder_results = fullfile(folder_imgs, [size_imgs '\']);
if(~exist(folder_results, 'dir')), mkdir(folder_results); end;

extension = '.bmp'; %'.jpg';


% list all 'extension' files in 'folder_imgs'
img_filenames = dir([folder_imgs 'CFD' '*' extension]); % 'CFD' to avoid masks_
n_img_filenames = numel(img_filenames);

hw = waitbar(0, 'Resizing images... 0%');
t=tic;

for nf=1:n_img_filenames

    waitbar(nf/n_img_filenames, hw, sprintf('Resizing images... %.2f%%', nf/n_img_filenames*100));
    
    % % Image filename
    filename = img_filenames(nf).name;
    
    img = imread(fullfile(folder_imgs, filename));
    
    img_8bits = imresize(img, size8bits);
    
    imwrite(img_8bits, fullfile(folder_results, filename));
end
close(hw)