function folder_results = crop_by_coordinates_same_size(folder_imgs, extension)
% ALL IMAGES NEED TO BE THE SAME SIZE TO APPLY THIS FUNCTION TO THEM

% folder_imgs = 'C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\cfd-boy-neutral-allraces\ewm-iris-aligned-MAX-size\same-size-top-cropped\290x168\';
% folder_results = fullfile(folder_imgs, 'cropped-200x110\');
% 
% % test to check pipeline flow
% folder_imgs = 'D:\DOCTORADO\01_DATA\_NoGIT_common_dbs\cfd\_ewm-iris-aligned-MAX-size\_same-size-top-cropped\_290x168\';
% folder_results = fullfile(folder_imgs, 'cropped-236x126_pipeline-test\');

folder_results = fullfile(folder_imgs, 'cropped-236x126\');
if(~exist(folder_results, 'dir')), mkdir(folder_results); end

% extension = '.bmp';

% list all 'extension' files in 'folder_imgs'
img_filenames = dir([folder_imgs '\*' extension]);
n_img_filenames = numel(img_filenames);

% start the loop for every image in the list to resize images
% hw = waitbar(0, 'Cropping images... 0%');
t=tic;
parfor nf=1:n_img_filenames
    
    %waitbar(nf/n_img_filenames, hw, sprintf('Cropping images... %.2f%%', nf/n_img_filenames*100));
    %cprintf('blue', ['Cropping image ' num2str(nf) '/' num2str(n_img_filenames) '\n']);
    fprintf('Cropping image %d/%d\n', nf, n_img_filenames);
    
    % % Image filename
    filename = strrep(img_filenames(nf).name, extension, '');
    
    % % Load image
    img = imread(fullfile(folder_imgs, [filename extension]));
    
    % % Crop the image
    cropped_img = imcrop(img,[35 15 235 125]);
    % to test pipeline flow. ITS OK, THESE SETTINGS PRODUCE A 236x126
    
    % % Save image       
    imwrite(cropped_img, fullfile(folder_results, strcat(filename, extension)));

end
disp('Execution ended.');
toc(t)

% close(hw);